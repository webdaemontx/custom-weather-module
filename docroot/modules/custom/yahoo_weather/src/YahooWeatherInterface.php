<?php
/**
 * @file
 * Contains \Drupal\yahoo_weather\YahooWeatherInterface.
 */

namespace Drupal\yahoo_weather;

/**
 * Yahoo Weather Interface.
 */
interface YahooWeatherInterface
{

  /**
   * Get location data.
   * @param null $location
   * @param string $filter
   * @param string $unit
   */
  public function locationCheck($location = NULL, $filter = '', $unit = 'f');

  /**
   * Check Day or Night.
   * @param $date
   * @param $sunrise
   * @param $sunset
   */
  public static function checkDayNight($date, $sunrise, $sunset);

  /**
   * Get Wind Direction.
   * @param $direction
   */
  public static function windDirection($direction);

  /**
   * buildBaseString.
   * @param $baseURI
   * @param $method
   * @param $params
   */
  public static function buildBaseString($baseURI, $method, $params);

  /**
   * buildAuthorizationHeader.
   * @param $oauth
   */
  public static function buildAuthorizationHeader($oauth);

}
