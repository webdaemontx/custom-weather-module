<?php

namespace Drupal\yahoo_weather\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;

/**
 * Returns responses for YahooWeather routes.
 */
class YahooWeatherController extends ControllerBase {

  /**
   * The form builder service.
   *
   * @var FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The configuration factory service.
   *
   * @var ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Construct a yahoo_weather controller object.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory;
   *   The configuration factory resource settings.
   */
public function __construct(FormBuilderInterface $form_builder, ConfigFactoryInterface $config_factory) {
  $this->formBuilder = $form_builder;
    $this->configFactory = $config_factory;
}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('config.factory')
    );
  }

  /**
   * Construct the locations.
   */
  public function locationList() {
    $rows = $build = [];
    $location_list = $this->configFactory
      ->get('yahoo_weather.location')
      ->get('location');

    $form_arg = 'Drupal\yahoo_weather\Form\YahooWeatherForm';
    $build['yahoo_weather_form'] = $this->formBuilder->getForm($form_arg);

    $header = [
      $this->t('Location'),
      [
        'data' => $this->t('Operations'),
        'colspan' => 2,
      ],
    ];

    if (!empty($location_list)) {
      foreach ($location_list as $key => $value) {
        $operations = [];
        $operations['delete'] = [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute('yahoo_weather.delete', ['location' => $key]),
        ];

        $data['location'] = $key;
        $data['location'] .= Html::escape($value);
        $data['operations'] = [
          '#type' => 'operations',
          '#links' => $operations,
        ];

        $rows[] = $data;
      }
    }

  /**
   * Builds the response.
   */
  $build['yahoo_weather_table'] = [
    '#type' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => $this->t('No locations available.'),
  ];

return $build;
  }
}
