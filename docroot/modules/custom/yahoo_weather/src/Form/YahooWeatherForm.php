<?php
/**
 * @file
 * Contains \Drupal\yahoo_weather\Form\YahooWeatherForm.
 */

namespace Drupal\yahoo_weather\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\yahoo_weather\YahooWeatherInterface;

/**
 * Controller location for Yahoo Weather Form.
 */
class YahooWeatherForm extends ConfigFormBase {

  /**
   * The Drupal configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Yahoo Weather control.
   *
   * @var Drupal\yahoo_weather\YahooWeatherInterface
   */
  protected $yahooWeather;
  /**
   * Constructs a location form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory holding resource settings.
   * @param \Drupal\yahoo_weather\yahooWeatherInterface $yahoo_weather
   *   The controls of Yahoo Weather.
   */
  public function __construct(ConfigFactoryInterface $config_factory, YahooWeatherInterface $yahoo_weather) {
    $this->configFactory = $config_factory;
    $this->yahooWeather = $yahoo_weather;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('yahoo_weather.controller')
    );
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::getFormID().
   */
  public function getFormId() {
    return 'yahoo_weather_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'yahoo_weather.location',
    ];
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['location'] = array(
      '#type' => 'textfield',
      '#title' => 'Location',
      '#description' => t('Enter your location using city, state or just city.'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $locations = $this->configFactory->get('yahoo_weather.location')->get('location');
    $location_value = $form_state->getValue('location');
    //if (is_numeric($location_value))) {
    if (empty($location_value)) {
      $form_state->setErrorByName('location', $this->t('location is empty.'));
    }
    elseif (!empty($locations) && array_key_exists($location_value, $locations)) {
      $form_state->setErrorByName('location', $this->t('location already exists.'));
    }
    elseif (!empty($location_value)) {
      $output = $this->yahooWeather->locationCheck($location_value, 'location', 'f');
      if (!empty($output)) {
        if (isset($output['location']) && !empty($output['location'])) {
          $city = $output['location']['city'] . ',' . $output['location']['region'] . ',' . $output['location']['country'];
          $locations[$location_value] = $city;
          $this->config('yahoo_weather.location')
            ->set('location', $locations)
            ->save();
        }
        else {
          $form_state->setErrorByName('location', $this->t('location invalid.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }
}
